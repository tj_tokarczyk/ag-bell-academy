<?php $color = hex2rgb(get_sub_field('content_background_color')); ?>
<style>
  .content.color p{
    color:<?php the_sub_field('content_copy_color'); ?>;
  }
  .content.color{
    background-color:rgba(<?php echo $color[red]; ?>, <?php echo $color[green]; ?>, <?php echo $color[blue]; ?>, 0.80);
    color:<?php the_sub_field('content_copy_color'); ?>;
  }
</style>

<div class="full-img d-flex align-items-end parallax-window" data-parallax="scroll" data-image-src="<?php the_sub_field('background_image'); ?>">
  <div class="container ">
    <div class="row">
      <div class="col-lg p-4 content color">
        <?php the_sub_field('content'); ?>
      </div>
      <div class="col-lg"></div>
    </div>
  </div>
</div>
