<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-1 col-sm-0"></div>
      <div class="col-lg-12 col-md-10 col-sm-12">
        <?php the_sub_field('content'); ?>
      </div>
    </div>
  </div>
</div>
