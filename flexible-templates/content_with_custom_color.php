<style>
  .opposite{
    padding-top:50px;
    padding-bottom:50px;
  }
  .opposite h1,.opposite h2,.opposite h3,.opposite h4,.opposite h5,.opposite h6,.opposite p,.opposite strong,.opposite a,.opposite td,.opposite table,.opposite tr,.opposite th{
    color:<?php the_sub_field('copy_color'); ?>;
  }
  .opposite th{
    background-color:<?php the_sub_field('copy_color'); ?>;
    color:<?php the_sub_field('bg_color'); ?>;
    border:3px solid <?php the_sub_field('bg_color'); ?>;
    border-bottom:none;
    border-top:none;
    border-left:3px solid <?php the_sub_field('copy_color'); ?>;
  }
  .opposite th:last-of-type{
    border-right:3px solid <?php the_sub_field('copy_color'); ?>;
  }
  .opposite td{
    border:3px solid <?php the_sub_field('copy_color'); ?>;
  }
  .tabled-bg{
    background-color:<?php the_sub_field('bg_color'); ?>;
    <?php if( get_sub_field('display_bottom_padding') == 'Yes' ) : ?>
      padding-bottom:200px;
    <?php endif; ?>
  }
</style>

<div class="tabled-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-1 col-sm-0"></div>
      <div class="col-lg-12 col-md-10 col-sm-12 opposite">
        <?php if( get_sub_field('icon')) { ?>
          <p class="text-center">
            <img src="<?php the_sub_field('icon'); ?>"  />
          </p>
        <?php } ?>
        <?php the_sub_field('html_content_copy'); ?>
        <?php if(get_sub_field( 'table' )) : ?>
        <?php $table = get_sub_field( 'table' );
          if ( $table ) {
              echo '<table border="0">';
                  if ( $table['header'] ) {
                      echo '<thead>';
                          echo '<tr>';
                              foreach ( $table['header'] as $th ) {
                                  echo '<th>';
                                      echo $th['c'];
                                  echo '</th>';
                              }
                          echo '</tr>';
                      echo '</thead>';
                  }
                  echo '<tbody>';
                      foreach ( $table['body'] as $tr ) {
                          echo '<tr>';
                              foreach ( $tr as $td ) {
                                  echo '<td>';
                                      echo $td['c'];
                                  echo '</td>';
                              }
                          echo '</tr>';
                      }
                  echo '</tbody>';
              echo '</table>';
          }
        ?>
      <?php endif; ?>
      </div>
    </div>
  </div>
</div>
