<?php if( get_sub_field('display') == "Top" ){
        $display = "align-items-start";

      } else {
        $display = "align-items-end";
      }
?>
<div class="full-img d-flex <?php echo $display; echo " "; the_sub_field('display'); ?> parallax-window" data-parallax="scroll" data-image-src="<?php the_sub_field('background_image'); ?>" style="height:<?php the_sub_field('custom_image_height'); ?>px;">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <?php the_sub_field('html_content'); ?>
    </div>
  </div>
</div>
