<style>
  .inline-button-style{
    border:1px solid <?php echo $button['button_color']; ?>
  }
</style>

<div class="container margin">
  <div class="row d-sm-flex justify-content-center">
    <?php
      // check if the repeater field has rows of data
      if( have_rows('column') ):

       	// loop through the rows of data
          while ( have_rows('column') ) : the_row();

              ?>
                <?php if( count(get_sub_field('button')) == 3 || count(get_sub_field('button')) == 2 ) : ?>
                  <div class="col-md-3 col-sm-12 col-xs-12 d-md-flex align-items-stretch">
                <?php else : ?>
                  <div class="col-md-3 col-sm-6 col-xs-12 d-md-flex align-items-stretch">
                <?php endif;  ?>


                  <div class="custom-card">
                    <div class="custom-card-img">
                      <img src="<?php the_sub_field('image'); ?>" />
                    </div>
                    <div class="custom-card-content">
                      <div>
                        <?php the_sub_field('html_content'); ?>
                        <?php $button = get_sub_field('button'); ?>
                      </div>
                      <div>
                        <p><button class="inline-button-style"><a href="<?php echo $button['button_link']; ?>"><strong><?php echo $button['button_text']; ?></strong></a></button></p>
                      </div>
                    </div>
                  </div>
                </div>
              <?php

          endwhile;

      else :

          // no rows found

      endif;

      ?>
  </div>
</div>
