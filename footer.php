<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>
<?php if( is_front_page() ){ ?>
<!-- <div class="wrapper bg-dark pull-up">
	<p>&nbsp;</p>
</div>

<div class="wrapper bg-dark">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">
			<div class="col-sm">
				<h4>Do you have questions about the Academy or LSLS certification? Contact us.</h4>
			</div>
		</div>
		<div class="row">
			<p>&nbsp;</p>
		</div>
		<div class="row">
			<div class="col-sm">
				<p><strong>The AG Bell Academy for Listening and Spoken Language<br />3417 Volta Place NW Washington, D.C. 20007</strong></p>
				<p><strong>Phone:</strong> 202-204-4007<br /><strong>Fax:</strong> 202-337-8314<br /><strong>Email:</strong> <a href="mailto:academy@agbell.org">academy@agbell.org</a></p>
			</div>
			<div id="contact" class="col-sm">
				<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
			</div>
		</div>

	</div>

</div> -->
<?php } ?>
<div class="bg-dark" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row d-flex align-items-center">

			<div class="col">
				<p><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/06/ice.png" style="width:200px;"  /></p>
			</div>

			<div class="col">
				<p>The AG Bell Academy for Listening and Spoken Language is a proud member of the Institute for Credentialing Excellence.</p>
				<p>The AG Bell Academy for Listening and Spoken Language is a 501(c)(6) nonprofit organization. </p>
			</div>

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
