<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div id="page-wrapper">

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						// check if the flexible content field has rows of data
						if( have_rows('builder') ):

						     // loop through the rows of data
						    while ( have_rows('builder') ) : the_row();

						         get_template_part( 'flexible-templates/'. get_row_layout() );

						    endwhile;

						else :

						    // no layouts found

						endif;

						?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- #primary -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
